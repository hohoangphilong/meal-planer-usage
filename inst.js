const fileName = __dirname + "/prevdata-w-inst.json";
const inst = __dirname + "/inst.json";
const fs = require("fs");

const run = () => {
	fs.readFile(fileName, (err, data) => {
		if (err) throw err;
		let recipes = JSON.parse(data).recipes;

		fs.readFile(inst, (err, instData) => {
			if (err) throw err;

			let inst = JSON.parse(instData).instructions;

			const instLen = inst.length;

			recipes.map((item, index) => {
				let randInst = inst[Math.floor(Math.random() * instLen)];
				item.instructions = randInst;
			});

			fs.writeFile(
				fileName,
				JSON.stringify({ recipes }),
				"utf-8",
				(err, data) => {
					if (err) throw err;
					console.log(`Saved to file ${fileName}`);
				}
			);

			console.log("recipes[0] :", recipes[0]);
		});
	});
};

run();
