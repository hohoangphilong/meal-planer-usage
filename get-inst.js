const url = "https://www.themealdb.com/api/json/v1/1/search.php?f=";
const letter = "abcdefghijklmnopqrstuvwxyz";
const fetch = require("node-fetch");
const fs = require("fs");
const fileName = __dirname + "/inst.json";

const getData = () => {
	let instructions = [];
	letter.split("").map(async e => {
		try {
			const response = await fetch(url + e);
			const json = await response.json();
			const data = json.meals || [];
			const inst = data.map(e => e.strInstructions);
			instructions = [...instructions, ...inst];
		} catch (error) {
			console.log(error);
		}
	});
	setTimeout(() => {
		fs.writeFile(
			fileName,
			JSON.stringify({ instructions }),
			"utf-8",
			(err, data) => {
				if (err) throw err;
				console.log(`Saved to file ${fileName}`);
			}
		);
	}, 5000);
};

getData();
