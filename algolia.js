const fs = require("fs");
const fileName = __dirname + "/algolia.json";

const run = () => {
	fs.readFile(fileName, (err, data) => {
		if (err) throw err;
		const recipes = JSON.parse(data).recipes;

		// recipes.map((e, i) => {
		// 	delete e.cookTime;
		// 	delete e.datePublished;
		// 	delete e.description;
		// 	delete e.ingredients;
		// 	delete e.prepTime;
		// 	delete e.recipeYield;
		// 	delete e.instructions;

		// 	e.id = i;
		// });

		fs.writeFile(fileName, JSON.stringify(recipes), "utf-8", (err, data) => {
			if (err) throw err;
			console.log(`Saved to file ${fileName}`);
		});
	});
};

run();
