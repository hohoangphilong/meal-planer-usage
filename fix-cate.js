const fs = require("fs");
const fileName = __dirname + "/data-w-cate.json";
const cate = [
	"Beef",
	"Chicken",
	"Dessert",
	"Lamb",
	"Miscellaneous",
	"Pasta",
	"Pork",
	"Seafood",
	"Side",
	"Starter",
	"Vegan",
	"Vegetarian",
	"Breakfast",
	"Goat"
]; //14
const cuisine = [
	"Italian",
	"Indian",
	"Turkish",
	"Mexican",
	"Thai",
	"Greek",
	"Chinese",
	"Japanese",
	"French",
	"Spanish"
]; //10

const run = () => {
	fs.readFile(fileName, (err, data) => {
		if (err) {
			throw err;
		}
		const recipes = JSON.parse(data).recipes;

		recipes.map(e => {
			let randCate = Math.floor(Math.random() * 3 + 1);
			let randCui = Math.floor(Math.random() * 2 + 1);

			for (let i = 0; i < randCate; i++) {
				let rand = Math.floor(Math.random() * 14);
				e[cate[rand]] = true;
			}

			for (let i = 0; i < randCui; i++) {
				let rand = Math.floor(Math.random() * 10);
				e[cuisine[rand]] = true;
			}

			delete e.cuisine;
			delete e.category;
		});

		fs.writeFile(
			fileName,
			JSON.stringify({ recipes }),
			"utf-8",
			(err, data) => {
				if (err) throw err;
				console.log(`Saved to file ${fileName}`);
			}
		);
	});
};

run();
