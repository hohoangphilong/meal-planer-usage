const url = "https://www.themealdb.com/api/json/v1/1/categories.php";
const fetch = require("node-fetch");

const getData = async url => {
	try {
		const response = await fetch(url);
		const json = await response.json();
		console.log(json);
	} catch (error) {
		console.log(error);
	}
};

getData(url);
