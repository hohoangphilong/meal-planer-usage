const fileName = __dirname + "/bf-fix-rcp-id.json";
const fs = require("fs");
const uuid = require("uuid/v4");

const run = () => {
	fs.readFile(fileName, (err, data) => {
		if (err) throw err;
		let recipes = JSON.parse(data).recipes;

		let values = Object.values(recipes);

		let newRecipes = {};

		values.map(e => {
			let id = uuid();
			newRecipes[id] = e;
			newRecipes[id].uuid = id;
		});

		fs.writeFile(
			fileName,
			JSON.stringify({ recipes: newRecipes }),
			"utf-8",
			(err, data) => {
				if (err) throw err;
				console.log(`Saved to file ${fileName}`);
			}
		);
	});
};

run();
