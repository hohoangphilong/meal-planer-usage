const fileName = __dirname + "/prevdata.json";
const fs = require("fs");
const { parse } = require("recipe-ingredient-parser");
const cate = [
  "Beef",
  "Chicken",
  "Dessert",
  "Lamb",
  "Miscellaneous",
  "Pasta",
  "Pork",
  "Seafood",
  "Side",
  "Starter",
  "Vegan",
  "Vegetarian",
  "Breakfast",
  "Goat"
];
const time = ["5min", "10min", "15min", "20min", "30min", "45min", "60min"];
const cuisine = [
  "Italian",
  "Indian",
  "Turkish",
  "Mexican",
  "Thai",
  "Greek",
  "Chinese",
  "Japanese",
  "French",
  "Spanish"
];

const run = () => {
  fs.readFile(fileName, (err, data) => {
    if (err) throw err;
    let recipes = JSON.parse(data).recipes;

    recipes.map((item, index) => {
      item.category = cate[index % 14];
      item.prepTime = time[index % 7];
      item.cookTime = time[index % 7];
      item.recipeYield = (index + 1) % 4;
      item.cuisine = cuisine[index % 10];

      let array = item.ingredients.split("\n");
      let newArray = [];
      for (let i = 0, len = array.length; i < len; i++) {
        try {
          newArray.push(parse(array[i]));
        } catch (error) {
          newArray.push({
            quantity: null,
            unit: null,
            ingredient: array[i]
          });
        }
      }

      item.ingredients = newArray;
    });

    fs.writeFile(
      fileName,
      JSON.stringify({ recipes }),
      "utf-8",
      (err, data) => {
        if (err) throw err;
        console.log(`Saved to file ${fileName}`);
      }
    );
    // console.log("recipes[0] :", recipes[0]);
  });
};

run();
