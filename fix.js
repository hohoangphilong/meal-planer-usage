const fileName = __dirname + "/prevdata-fix.json";
const fs = require("fs");
const path = require("path");
const { Tagger } = require("ingredient-phrase-tagger");
const tagger = new Tagger();
const model_filename = path.join(__dirname, "model.crfsuite");

const is_opened = tagger.open(model_filename);
console.log("File model is opened:", is_opened);

const cate = [
	"Beef",
	"Chicken",
	"Dessert",
	"Lamb",
	"Miscellaneous",
	"Pasta",
	"Pork",
	"Seafood",
	"Side",
	"Starter",
	"Vegan",
	"Vegetarian",
	"Breakfast",
	"Goat"
];
const time = ["5min", "10min", "15min", "20min", "30min", "45min", "60min"];
const cuisine = [
	"Italian",
	"Indian",
	"Turkish",
	"Mexican",
	"Thai",
	"Greek",
	"Chinese",
	"Japanese",
	"French",
	"Spanish"
];

const run = () => {
	fs.readFile(fileName, (err, data) => {
		if (err) throw err;
		let recipes = JSON.parse(data).recipes;

		recipes.map((item, index) => {
			let array = item.ingredients.map(e => e.ingredient);
			const newArray = tagger.tag(array);

			item.ingredients.map((e, i) => {
				e.name = newArray[i].name || "";
				e.input = newArray[i].input || "";
				delete e.ingredient;
			});
		});

		fs.writeFile(
			fileName,
			JSON.stringify({ recipes }),
			"utf-8",
			(err, data) => {
				if (err) throw err;
				console.log(`Saved to file ${fileName}`);
			}
		);
		// console.log("recipes[0] :", recipes[0]);
	});
};

run();
