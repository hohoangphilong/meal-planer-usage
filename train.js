const path = require("path");
const { Trainer } = require("ingredient-phrase-tagger");
const trainer = new Trainer();

const data_path = path.join(__dirname, "nyt-ingredients-snapshot-2015.csv");
const model_filename = path.join(__dirname, "model.crfsuite");

trainer.append(data_path, 20000, 0).then(() => {
	trainer.train(model_filename);
});
